# OpenML dataset: Aloi

https://www.openml.org/d/40906

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Markus Goldstein  
E. Schubert","R. Wojdanowski","A. Zimek","H.-P. Kriegel","On Evaluation of Outlier  Rankings and Outlier Scores","In Proceedings of the 12th SIAM International  Conference on Data Mining (SDM)","Anaheim","CA","2012.  
**Source**: [original](https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/OPQMVF) - Date unknown  
**Please cite**: &quot;J. M. Geusebroek, G. J. Burghouts, and A. W. M. Smeulders, The Amsterdam 
library of object images, Int. J. Comput. Vision, 61(1), 103-112, January, 
2005.&quot;  

&quot;The aloi dataset is derived from the &ldquo;Amsterdam Library of Object Images&rdquo; collection (see citation request). The original dataset contains about 110 images of 1000 small objects taken under different light conditions and viewing angles. From the original images, a 27 dimensional feature vector was extracted using HSB color histograms. Some objects were chosen as anomalies and the data was down-sampled such that the resulting dataset contains 50,000 instances including 3.02% anomalies. (cite from Goldstein, Markus, and Seiichi Uchida. &quot;A comparative evaluation of unsupervised anomaly detection algorithms for multivariate data.&quot; PloS one 11.4 (2016): e0152173.).  This dataset is not the original dataset from the provided URL. The target variable is renamed into &quot;Target&quot; and relabeled into &quot;Normal&quot; and &quot;Anomaly&quot;.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40906) of an [OpenML dataset](https://www.openml.org/d/40906). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40906/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40906/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40906/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

